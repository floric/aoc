const raw = `1,1,1,1,1,5,1,1,1,5,1,1,3,1,5,1,4,1,5,1,2,5,1,1,1,1,3,1,4,5,1,1,2,1,1,1,2,4,3,2,1,1,2,1,5,4,4,1,4,1,1,1,4,1,3,1,1,1,2,1,1,1,1,1,1,1,5,4,4,2,4,5,2,1,5,3,1,3,3,1,1,5,4,1,1,3,5,1,1,1,4,4,2,4,1,1,4,1,1,2,1,1,1,2,1,5,2,5,1,1,1,4,1,2,1,1,1,2,2,1,3,1,4,4,1,1,3,1,4,1,1,1,2,5,5,1,4,1,4,4,1,4,1,2,4,1,1,4,1,3,4,4,1,1,5,3,1,1,5,1,3,4,2,1,3,1,3,1,1,1,1,1,1,1,1,1,4,5,1,1,1,1,3,1,1,5,1,1,4,1,1,3,1,1,5,2,1,4,4,1,4,1,2,1,1,1,1,2,1,4,1,1,2,5,1,4,4,1,1,1,4,1,1,1,5,3,1,4,1,4,1,1,3,5,3,5,5,5,1,5,1,1,1,1,1,1,1,1,2,3,3,3,3,4,2,1,1,4,5,3,1,1,5,5,1,1,2,1,4,1,3,5,1,1,1,5,2,2,1,4,2,1,1,4,1,3,1,1,1,3,1,5,1,5,1,1,4,1,2,1`;
let fishes = raw
  .split(`,`)
  .map((val) => ({ [val]: 1 }))
  .reduce((a, b) => {
    const joined = {};
    Object.entries(a).forEach(([key, count]) => (joined[key] = count));
    Object.entries(b).forEach(
      ([key, count]) => (joined[key] = (joined[key] || 0) + count)
    );
    return joined;
  });

for (let d = 1; d <= 256; d++) {
  const newFishes = {};
  Object.entries(fishes).forEach(([key, count]) => {
    if (key === "0") {
      newFishes["6"] = (newFishes["6"] || 0) + count;
      newFishes["8"] = (newFishes["8"] || 0) + count;
    } else {
      newFishes[`${Number(key) - 1}`] =
        (newFishes[`${Number(key) - 1}`] || 0) + count;
    }
  });
  fishes = newFishes;
  console.log(
    "day " + d,
    Object.values(fishes).reduce((a, b) => a + b)
  );
}
