const raw = `6788383436
5526827441
4582435866
5152547273
3746433621
2465145365
6324887128
8537558745
4718427562
2283324746`;
const map = raw.split("\n").map((line) => line.split("").map((n) => Number(n)));

const increase = (x, y, map) => {
  const val = map[x][y];
  map[x][y] += 1;
};

const setVal = (x, y, map, val) => {
  map[x][y] = val;
};

const expand = (x, y, map) => {
  map[x][y] += 1;
  if (map[x][y] <= 9) {
    return;
  } else if (map[x][y] === 10) {
    if (x > 0 && y > 0) {
      expand(x - 1, y - 1, map);
    }
    if (x > 0) {
      expand(x - 1, y, map);
    }
    if (y > 0) {
      expand(x, y - 1, map);
    }
    if (x + 1 !== map.length) {
      expand(x + 1, y, map);
    }
    if (x + 1 !== map.length && y + 1 !== map[x].length) {
      expand(x + 1, y + 1, map);
    }
    if (y + 1 !== map[x].length) {
      expand(x, y + 1, map);
    }
    if (y + 1 !== map[x].length && x > 0) {
      expand(x - 1, y + 1, map);
    }
    if (x + 1 !== map.length && y > 0) {
      expand(x + 1, y - 1, map);
    }
  }
};

let totalFlashes = 0;
for (let i = 0; i < 900; i++) {
  // check influences
  for (let x = 0; x < map.length; x++) {
    for (let y = 0; y < map[x].length; y++) {
      expand(x, y, map);
    }
  }

  // reset
  let flashes = 0;
  for (let x = 0; x < map.length; x++) {
    for (let y = 0; y < map[x].length; y++) {
      const val = map[x][y];

      // flash
      if (val > 9) {
        setVal(x, y, map, 0);
        flashes++;
      }
    }
  }
  const fullFlash =
    map.flatMap((row) => row.filter((v) => v !== 0)).length === 0;
  console.log({ i: i + 1, totalFlashes: flashes, fullFlash });
  totalFlashes += flashes;
  if (fullFlash) {
    break;
  }
}
console.log(totalFlashes);
