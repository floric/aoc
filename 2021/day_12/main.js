const raw = `pq-GX
GX-ah
mj-PI
ey-start
end-PI
YV-mj
ah-iw
te-GX
te-mj
ZM-iw
te-PI
ah-ZM
ey-te
ZM-end
end-mj
te-iw
te-vc
PI-pq
PI-start
pq-ey
PI-iw
ah-ey
pq-iw
pq-start
mj-GX`;
const cave = raw
  .split("\n")
  .map((line) => line.split("-"))
  .flatMap(([from, to]) => [
    { from, to },
    { from: to, to: from },
  ]);

const isBigCave = (to) => to.toUpperCase() === to;
const isUnknown = (route, to) => route.find((d) => d === to) === undefined;
const isTwiceMatch = (twiceCave, route, to) =>
  twiceCave === null &&
  !isBigCave(to) &&
  !isUnknown(route, to) &&
  to !== "start" &&
  to !== "end";

const findRoutes = (cave, pos, route, twiceCave) => {
  const newRoute = [...route, pos];
  if (pos === "end") {
    return [newRoute];
  }
  return cave.flatMap(({ from, to }) => {
    const isAllowed =
      from === pos &&
      (isBigCave(to) ||
        isUnknown(route, to) ||
        isTwiceMatch(twiceCave, route, to));
    return isAllowed
      ? findRoutes(
          cave,
          to,
          newRoute,
          isTwiceMatch(twiceCave, route, to) ? to : twiceCave
        )
      : [];
  });
};

const start = new Date().getTime();
const routes = findRoutes(cave, "start", [], null);
console.log({ routes: routes.length, ms: new Date().getTime() - start });
