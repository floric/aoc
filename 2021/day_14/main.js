const raw = `OFSVVSFOCBNONHKFHNPK

HN -> C
VB -> K
PF -> C
BO -> F
PB -> F
OH -> H
OB -> N
PN -> O
KO -> V
CK -> V
FP -> H
PC -> V
PP -> N
FN -> N
CC -> F
FC -> N
BP -> N
SH -> F
NS -> V
KK -> B
HS -> C
NV -> N
FO -> B
VO -> S
KN -> F
SC -> V
NB -> H
CH -> B
SF -> V
NP -> V
FB -> P
CV -> B
PO -> P
SV -> P
OO -> V
PS -> C
CO -> N
SP -> B
KP -> H
KH -> S
KS -> S
NH -> K
SS -> P
PV -> P
KV -> V
ON -> N
BS -> C
HP -> K
SB -> P
VC -> B
HB -> N
FS -> V
VP -> K
BB -> N
FK -> S
CS -> P
SO -> F
HF -> F
VV -> C
BC -> S
SN -> K
KB -> H
BN -> H
HO -> S
KC -> F
CP -> S
HC -> S
OS -> K
NK -> N
BF -> S
VN -> B
SK -> K
HV -> B
KF -> H
FV -> B
VF -> H
BH -> S
NN -> O
HH -> K
CN -> H
PH -> V
NF -> S
OV -> P
OC -> V
OK -> H
OF -> H
HK -> N
FH -> P
BK -> N
VS -> H
NO -> V
VK -> K
CF -> N
CB -> N
NC -> K
PK -> B
VH -> F
FF -> C
BV -> P
OP -> K`;
const [templateRaw, stepsRaw] = raw.split("\n\n");
let pairs = {};
for (let i = 0; i < templateRaw.length - 1; i++) {
  const pair = templateRaw[i] + templateRaw[i + 1];
  pairs[pair] = (pairs[pair] || 0) + 1;
}
const steps = stepsRaw
  .split("\n")
  .map((line) => line.split(" -> "))
  .map(([from, to]) => ({ from, to }));

const totalSteps = 40;
for (let step = 0; step < totalSteps; step++) {
  let newPairs = {};
  for (var [part, count] of Object.entries(pairs)) {
    const match = steps.find(({ from }) => from === part);
    const leftPair = `${part[0]}${match.to}`;
    newPairs[leftPair] = (newPairs[leftPair] || 0) + count;
    const rightPair = `${match.to}${part[1]}`;
    newPairs[rightPair] = (newPairs[rightPair] || 0) + count;
  }
  pairs = newPairs;
}
const counts = Object.entries(pairs)
  .map(([key, val]) => ({ [key[0]]: val }))
  .reduce((a, b) => {
    const combined = b;
    Object.entries(a).forEach(([key, val]) => {
      combined[key] = (b[key] || 0) + val;
    });
    return combined;
  });

// add last missing character because of pairs
counts[templateRaw[templateRaw.length - 1]] += 1;

const mostCommon = Object.entries(counts).reduce((a, b) =>
  a[1] > b[1] ? a : a[1] === b[1] ? a : b
);
const leastCommon = Object.entries(counts).reduce((a, b) =>
  a[1] < b[1] ? a : a[1] === b[1] ? a : b
);
console.log({
  counts,
  totalSteps,
  mostCommon,
  leastCommon,
  result: mostCommon[1] - leastCommon[1],
});
