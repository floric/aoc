const raw = `target area: x=117..164, y=-140..-89`;
const areaParts = raw
  .slice(13)
  .split(", ")
  .map((part) =>
    part
      .split("=")[1]
      .split("..")
      .map((n) => Number(n))
  );
const area = { x: areaParts[0], y: areaParts[1] };
console.log({ area });

const launchProbe = (dir, start) => {
  let pos = start;
  let newDir = dir;
  let reached = false;
  let maxHeight = pos.y;
  for (let i = 0; i < 10000; i++) {
    pos = { x: pos.x + newDir.x, y: pos.y + newDir.y };
    newDir = {
      x:
        (Math.abs(newDir.x) - 1) * (newDir.x > 0 ? 1 : newDir.x === 0 ? 0 : -1),
      y: newDir.y - 1,
    };
    if (pos.y > maxHeight) {
      maxHeight = pos.y;
    }
    if (
      pos.x >= area.x[0] &&
      pos.x <= area.x[1] &&
      pos.y >= area.y[0] &&
      pos.y <= area.y[1]
    ) {
      reached = true;
      break;
    } else if (pos.x > area.x[1] && pos.y > area.y[1]) {
      reached = false;
      break;
    }
  }
  return { newDir, pos, reached, maxHeight };
};

const successes = [];
let firstHit = false;
for (let x = 0; x < 1000; x++) {
  for (let y = -1000; y < 1000; y++) {
    const res = launchProbe({ x, y }, { x: 0, y: 0 });
    if (res.reached) {
      successes.push(res);
      console.log({ x, y, height: res.maxHeight, total: successes.length });
    }
  }
}

const maxHeight = successes
  .map((s) => s.maxHeight)
  .reduce((a, b) => (a > b ? a : a === b ? a : b));
console.log({ maxHeight, total: successes.length });
