import lodash from "lodash";

const raw = `[[7,[8,[3,5]]],[[[3,6],9],1]]
[[[[1,7],8],[0,4]],[[[0,9],2],[2,[5,6]]]]
[[[4,[4,4]],[8,[4,4]]],[[5,4],6]]
[[[1,[1,3]],[[9,6],1]],[[4,[5,4]],[[4,4],[0,8]]]]
[[[[6,2],[2,5]],[2,1]],[[1,5],7]]
[[[[5,0],[2,7]],[[2,5],2]],[2,[3,2]]]
[[[6,[6,6]],[0,[2,8]]],[[8,[5,6]],[4,5]]]
[[[6,[3,5]],8],[[2,[7,0]],5]]
[[[[7,8],[3,6]],[1,6]],[[[4,2],1],[[0,7],[5,6]]]]
[8,2]
[[5,5],[[2,[9,1]],8]]
[[[4,8],[[1,8],2]],[9,2]]
[2,[[8,[8,3]],[0,6]]]
[[[6,6],[[6,0],6]],[0,[[3,4],3]]]
[[[[2,9],[5,9]],[2,[4,3]]],[6,0]]
[[[6,2],0],[7,7]]
[[[9,6],5],[2,[[0,1],[5,5]]]]
[[6,[[0,1],[5,1]]],5]
[4,[[[4,2],3],[2,[5,0]]]]
[[[7,9],2],2]
[[[5,[2,1]],1],[[1,1],[8,5]]]
[[[[5,9],0],[[1,9],0]],4]
[[7,[0,5]],[[0,3],[8,2]]]
[[6,[9,[7,7]]],6]
[2,[[1,[1,0]],[4,[6,1]]]]
[[0,6],[[[5,1],6],[[4,7],[8,0]]]]
[[[1,[4,7]],[0,[1,2]]],[[1,1],[[1,2],[1,3]]]]
[[8,[3,0]],[3,[1,[8,1]]]]
[[[7,[4,0]],[[8,7],2]],[[7,[7,3]],7]]
[3,[[1,7],2]]
[8,[[[1,5],0],1]]
[[6,[1,4]],7]
[[[[2,6],2],8],1]
[9,7]
[9,[[[1,1],1],[[3,0],[7,3]]]]
[[[[8,5],7],[[5,1],[6,4]]],[4,[[7,6],[2,7]]]]
[[[[8,7],1],0],[[9,9],[[1,0],8]]]
[[9,[[1,1],7]],[[3,0],4]]
[[[[8,2],4],[9,[7,9]]],[[0,2],[[3,0],5]]]
[[[[3,6],3],[[9,7],[0,6]]],[[[4,9],[1,3]],[2,[7,3]]]]
[[[[3,8],0],[[3,6],5]],[[3,[4,2]],[[6,1],[8,5]]]]
[[2,7],[[0,0],8]]
[[[[0,3],7],[2,0]],3]
[[0,2],[[[3,1],0],[0,0]]]
[[[[6,1],7],[[8,4],[2,4]]],[[1,6],[2,3]]]
[[2,[2,[9,1]]],[4,[[0,4],9]]]
[[[3,[5,6]],7],1]
[[[3,0],[8,[9,3]]],[[[1,1],1],[6,7]]]
[[[6,[4,4]],[[1,9],1]],[[[8,1],[9,8]],[[6,3],1]]]
[[[5,8],[[0,3],[1,7]]],[[[3,2],[4,7]],1]]
[[5,5],[[[8,3],[6,6]],2]]
[[[[1,9],[8,5]],[[7,7],8]],[0,[8,[7,4]]]]
[[6,[4,[4,3]]],[5,[6,[7,2]]]]
[[0,[[9,0],0]],5]
[[[[5,6],[1,3]],[[0,5],[7,5]]],[[[0,4],[3,6]],[8,[3,6]]]]
[[3,[[4,7],[7,0]]],[[[4,1],5],[[6,6],[7,4]]]]
[[[[4,3],[0,1]],[[7,3],[2,3]]],[[[3,7],[2,2]],[6,5]]]
[[1,1],[[[1,4],6],[6,[3,9]]]]
[[[[0,8],[2,0]],5],[4,[[6,1],[2,1]]]]
[[7,[3,[7,2]]],[[7,9],8]]
[[[4,[9,8]],[8,[3,2]]],[7,9]]
[[[4,[4,2]],[5,[0,3]]],[[[4,9],[2,9]],[[1,5],[0,8]]]]
[[1,[[9,8],0]],[5,[[4,3],5]]]
[[[[5,1],3],[[2,9],[9,0]]],[1,[6,3]]]
[[[6,4],[6,1]],7]
[[4,8],[[7,2],6]]
[[[5,[4,8]],[[1,7],[2,8]]],[6,[[8,4],[3,5]]]]
[[5,8],[[[4,0],[6,0]],[5,[6,0]]]]
[[3,[[5,3],8]],[8,5]]
[[[2,6],[1,[2,3]]],[[[1,7],[5,7]],[[0,0],[0,5]]]]
[[0,[[4,3],[3,6]]],[[2,[6,6]],[0,[2,9]]]]
[[[5,9],[6,2]],[[7,6],8]]
[[9,2],[1,[[0,5],[5,0]]]]
[[[3,1],[9,3]],3]
[[[[2,0],[4,2]],6],[[[5,2],[7,8]],[[0,7],3]]]
[[7,[[3,9],[6,3]]],[2,[[6,4],3]]]
[[5,[3,[7,4]]],[[2,5],[0,9]]]
[3,7]
[[3,9],[[[4,4],9],[[3,1],7]]]
[[[[4,0],1],[8,[3,6]]],[[9,[4,4]],[[9,9],9]]]
[[8,[[8,1],5]],[[[9,1],4],[[8,5],3]]]
[[6,[[6,3],[3,7]]],4]
[[[1,[0,8]],9],[[8,5],[3,[0,5]]]]
[[[[3,1],0],[[8,5],[1,0]]],[0,2]]
[[2,[4,7]],2]
[[[2,0],[2,2]],4]
[4,[[5,8],5]]
[[[2,[0,5]],[[3,3],[6,6]]],1]
[[[2,[2,4]],[5,[7,1]]],[3,5]]
[[2,[9,[3,9]]],9]
[[[7,[7,1]],[[5,2],1]],[[2,1],[9,[7,3]]]]
[[4,[4,6]],4]
[[[4,2],[9,[3,8]]],[[2,4],0]]
[[[7,[0,3]],4],[[[1,8],4],[[5,1],1]]]
[[[1,3],3],[[4,9],[[0,0],6]]]
[[[4,1],0],[[[5,6],[0,8]],[[2,1],1]]]
[[3,[3,[7,9]]],[[[6,8],8],[[7,9],3]]]
[[4,[[1,6],[4,6]]],[[1,8],[3,8]]]
[[[[5,9],2],[[6,7],4]],3]
[[[[2,1],[1,9]],7],[[[0,9],[0,5]],[[2,5],[5,0]]]]`;
const pairs = raw.split("\n").map(JSON.parse);

const explodeDepth = 4;
const typeLeaf = "leaf";
const typeBranch = "branch";
const dirLeft = "l";
const dirRight = "r";

const reduce = (pair) => {
  const tree = parsePair(pair);
  let applied = true;
  while (applied) {
    applied = explode(tree);
    if (applied) {
      continue;
    }
    applied = split(tree);
  }
  return printTree(tree);
};

const parsePair = (pair) => parsePairRec(pair, []);

const parsePairRec = (pair, path) =>
  typeof pair === "number"
    ? { val: pair, path, t: typeLeaf }
    : {
        t: typeBranch,
        path,
        l: parsePairRec(pair[0], [...path, dirLeft]),
        r: parsePairRec(pair[1], [...path, dirRight]),
      };

const printTree = (tree) =>
  tree.t === typeLeaf ? tree.val : [printTree(tree.l), printTree(tree.r)];

const pathsMatch = (a, b) => JSON.stringify(a) === JSON.stringify(b);

const traverseToRec = (tree, path, depth) => {
  if (pathsMatch(tree.pathl, path)) {
    return tree;
  }
  const nextNode = tree[path[depth]];
  return nextNode ? traverseToRec(nextNode, path, depth + 1) : null;
};

const findNeighbourLeaf = (tree, path, dir) => {
  const flatNodes = flattenTreeRec(tree);
  for (let i = 0; i < flatNodes.length; i++) {
    const matches = pathsMatch(flatNodes[i].path, path);
    if (matches && dir === dirLeft) {
      return i > 0 ? flatNodes[i - 1] : null;
    } else if (matches) {
      return i < flatNodes.length - 1 ? flatNodes[i + 1] : null;
    }
  }
  return null;
};

const flattenTreeRec = (node) =>
  node.t === typeLeaf
    ? [node]
    : [...flattenTreeRec(node.l), ...flattenTreeRec(node.r)];

const split = (tree) => {
  const toSplit = findSplitCandidateRec(tree)[0];
  if (!toSplit) {
    return false;
  }
  const newBranch = {
    t: typeBranch,
    path: toSplit.path,
    l: {
      t: typeLeaf,
      val: Math.floor(toSplit.val / 2),
      path: [...toSplit.path, dirLeft],
    },
    r: {
      t: typeLeaf,
      val: Math.ceil(toSplit.val / 2),
      path: [...toSplit.path, dirRight],
    },
  };
  lodash.set(tree, toSplit.path.join("."), newBranch);
  return true;
};

const explode = (tree) => {
  const toExplode = findExplodeCandidateRec(tree)[0];
  if (!toExplode) {
    return false;
  }
  const leftNeighbour = findNeighbourLeaf(
    tree,
    [...toExplode.path, dirLeft],
    dirLeft
  );
  if (leftNeighbour) {
    const newLeaf = {
      ...leftNeighbour,
      val: leftNeighbour.val + toExplode.l.val,
    };
    lodash.set(tree, leftNeighbour.path.join("."), newLeaf);
  }
  const rightNeighbour = findNeighbourLeaf(
    tree,
    [...toExplode.path, dirRight],
    dirRight
  );
  if (rightNeighbour) {
    const newLeaf = {
      ...rightNeighbour,
      val: rightNeighbour.val + toExplode.r.val,
    };
    lodash.set(tree, rightNeighbour.path.join("."), newLeaf);
  }
  const newLeaf = {
    t: typeLeaf,
    path: toExplode.path,
    val: 0,
  };
  lodash.set(tree, toExplode.path.join("."), newLeaf);

  return true;
};

const findExplodeCandidateRec = (tree) => {
  if (tree.t === typeLeaf) {
    return [];
  } else if (
    tree.path.length === explodeDepth &&
    tree.l.t === typeLeaf &&
    tree.r.t === typeLeaf
  ) {
    return [tree];
  }

  return [
    ...findExplodeCandidateRec(tree.l),
    ...findExplodeCandidateRec(tree.r),
  ];
};

const findSplitCandidateRec = (tree) => {
  if (tree.t === typeLeaf && tree.val >= 10) {
    return [tree];
  } else if (tree.t === typeLeaf) {
    return [];
  }

  return [...findSplitCandidateRec(tree.l), ...findSplitCandidateRec(tree.r)];
};

const add = (a, b) => reduce([a, b]);

const calculateMagnitude = (tree) =>
  tree.t === typeLeaf
    ? tree.val
    : calculateMagnitude(tree.l) * 3 + calculateMagnitude(tree.r) * 2;

const doHomework = () => {
  let sum = pairs[0];
  for (let i = 1; i < pairs.length; i++) {
    sum = add(sum, pairs[i]);
  }

  let bestMagnitude = 0;
  for (let a = 0; a < pairs.length; a++) {
    for (let b = 0; b < pairs.length; b++) {
      if (a !== b) {
        const localSum = add(pairs[a], pairs[b]);
        const magnitude = calculateMagnitude(parsePair(localSum));
        if (magnitude > bestMagnitude) {
          bestMagnitude = magnitude;
        }
      }
    }
  }

  console.log({
    sum: JSON.stringify(sum),
    bestMagnitude,
    magnitude: calculateMagnitude(parsePair(sum)),
  });
};

doHomework();
