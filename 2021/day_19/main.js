const fs = require("fs");
const path = require("path");

const radians = new Array(4).fill(0).map((_, i) => (i * Math.PI) / 2);
const directions = Array.from(
  new Map(
    radians
      .flatMap((alpha) =>
        radians.flatMap((beta) =>
          radians.flatMap((gamma) => {
            const xFactorX = Math.cos(alpha) * Math.cos(beta);
            const xFactorY =
              Math.cos(alpha) * Math.sin(beta) * Math.sin(gamma) -
              Math.sin(alpha) * Math.cos(gamma);
            const xFactorZ =
              Math.cos(alpha) * Math.sin(beta) * Math.cos(gamma) +
              Math.sin(alpha) * Math.sin(gamma);
            const yFactorX = Math.sin(alpha) * Math.cos(beta);
            const yFactorY =
              Math.sin(alpha) * Math.sin(beta) * Math.sin(gamma) +
              Math.cos(alpha) * Math.cos(gamma);
            const yFactorZ =
              Math.sin(alpha) * Math.sin(beta) * Math.cos(gamma) -
              Math.cos(alpha) * Math.sin(gamma);
            const zFactorX = -Math.sin(beta);
            const zFactorY = Math.cos(beta) * Math.sin(gamma);
            const zFactorZ = Math.cos(beta) * Math.cos(gamma);
            return [
              ({ x, y, z }) => ({
                x: Math.round(xFactorX * x + xFactorY * y + xFactorZ * z),
                y: Math.round(yFactorX * x + yFactorY * y + yFactorZ * z),
                z: Math.round(zFactorX * x + zFactorY * y + zFactorZ * z),
              }),
            ];
          })
        )
      )
      .map((fn) => {
        const { x, y, z } = fn({ x: 1, y: 2, z: 3 });
        return [`${x}.${y}.${z}`, fn];
      })
  ).values()
);

const transformPoints = (values, dirFn) => values.map(dirFn);

const addPoints = (a, b) => ({
  x: a.x + b.x,
  y: a.y + b.y,
  z: a.z + b.z,
});

const compareScanners = (newId, knownId, allScanners) => {
  const knownValues = allScanners.get(knownId);
  const newValues = allScanners.get(newId);
  for (let dirFn of directions) {
    const localBeacons = transformPoints(newValues, dirFn);
    const distances = new Map();
    for (let beaconA of knownValues) {
      for (let beaconB of localBeacons) {
        const distance3d = {
          x: beaconA.x - beaconB.x,
          y: beaconA.y - beaconB.y,
          z: beaconA.z - beaconB.z,
        };
        const distanceKey = `${distance3d.x},${distance3d.y},${distance3d.z}`;
        distances.set(distanceKey, (distances.get(distanceKey) || 0) + 1);
      }
    }
    const matches = Array.from(distances.entries())
      .filter(([_, count]) => count >= 12)
      .map(([key]) => key);
    if (matches.length > 0) {
      const offset = matches[0].split(",").map((val) => Number(val));
      return {
        dirFn,
        localBeacons,
        offset: {
          x: offset[0],
          y: offset[1],
          z: offset[2],
        },
      };
    }
  }
};

const parseInput = (filePath) => {
  const raw = fs.readFileSync(path.resolve(__dirname, filePath)).toString();
  const scanners = raw
    .split(/--- scanner \d* ---/)
    .filter((content) => !!content)
    .map((scanner, id) => ({
      id,
      values: scanner
        .split("\n")
        .filter((line) => !!line)
        .map((line) => line.split(",").map((val) => Number(val)))
        .map(([x, y, z]) => ({ x, y, z })),
    }));
  console.log(
    `found ${scanners.length} scanners with ${scanners
      .map((s) => s.values.length)
      .join(", ")} beacons`
  );
  return scanners;
};

const getDistance = (a, b) =>
  Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z);

const calculateMaxDistance = (knownScanners) => {
  let maxDistance = 0;
  for (let a = 0; a < knownScanners.size; a++) {
    for (let b = a + 1; b < knownScanners.size; b++) {
      const distance = getDistance(
        knownScanners.get(a).pos,
        knownScanners.get(b).pos
      );
      if (distance > maxDistance) {
        maxDistance = distance;
      }
    }
  }
  return maxDistance;
};

const parsedScanners = parseInput("input_b.txt");
const unknownScanners = new Set();
const beacons = new Set();
const allScanners = new Map();
parsedScanners.forEach(({ id, values }) => {
  allScanners.set(id, values);
  if (id > 0) {
    unknownScanners.add(id);
  }
});
const knownScanners = new Map();
knownScanners.set(0, {
  pos: { x: 0, y: 0, z: 0 },
  dirFn: ({ x, y, z }) => ({ x, y, z }),
});
allScanners.get(0).forEach(({ x, y, z }) => beacons.add(`${x}.${y}.${z}`));

const start = new Date().getTime();
while (unknownScanners.size > 0) {
  for (let newId of unknownScanners) {
    for (let [knownId, { pos, dirFn: knownDirFn }] of knownScanners.entries()) {
      const match = compareScanners(newId, knownId, allScanners);
      if (match) {
        const { localBeacons, offset, dirFn: newDirFn } = match;
        console.log(
          `match between ${newId} and ${knownId} for ${localBeacons.length} beacons`
        );
        localBeacons.forEach((localPt) => {
          const { x, y, z } = addPoints(
            knownDirFn(addPoints(localPt, offset)),
            pos
          );
          beacons.add(`${x}.${y}.${z}`);
        });
        unknownScanners.delete(newId);
        knownScanners.set(newId, {
          pos: addPoints(knownDirFn(offset), pos),
          dirFn: (pt) => knownDirFn(newDirFn(pt)),
        });
        console.log("determined scanner position", {
          id: newId,
          pos: knownScanners.get(newId).pos,
        });
        break;
      }
    }
  }
}

console.log({
  beacons: beacons.size,
  durationInMs: new Date().getTime() - start,
  maxDistance: calculateMaxDistance(knownScanners),
});
