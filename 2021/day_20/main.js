const fs = require("fs");
const path = require("path");

const importInput = (filePath) => {
  const raw = fs.readFileSync(path.resolve(__dirname, filePath)).toString();
  const lines = raw.split("\n");
  const algorithm = lines[0];
  const image = lines.slice(2).map((line) => line.split(""));
  return { algorithm, image };
};

const dark = ".";
const light = "#";
const matcherSize = 3;

const getPixel = ({ x, y }, image, isAlternatingBackground) =>
  x < 0 || x >= image[0].length || y < 0 || y >= image.length
    ? isAlternatingBackground
      ? light
      : dark
    : image[y][x];

const enhanceImage = (image, pass, algorithm) => {
  const newImage = [];
  const isAlternatingBackground = algorithm[0] === light && pass % 2 === 1;
  for (let y = -matcherSize + 1; y < image.length + matcherSize - 1; y++) {
    const row = [];
    for (let x = -matcherSize + 1; x < image[0].length + matcherSize - 1; x++) {
      const pixelValue = parseInt(
        new Array(matcherSize)
          .fill(0)
          .flatMap((_, localY) =>
            new Array(matcherSize).fill(0).map((_, localX) =>
              getPixel(
                {
                  x: x + localX - 1,
                  y: y + localY - 1,
                },
                image,
                isAlternatingBackground
              )
            )
          )
          .map((val) => (val === light ? "1" : "0"))
          .join(""),
        2
      );
      row.push(algorithm[pixelValue]);
    }
    newImage.push(row);
  }
  return newImage;
};

const getLitPixelCount = (image) =>
  image
    .slice(matcherSize - 2, -(matcherSize - 2))
    .flatMap((line) =>
      line
        .slice(matcherSize - 2, -(matcherSize - 2))
        .filter((val) => val === light)
    ).length;

let { algorithm, image } = importInput("input_b.txt");

for (let i = 0; i < 50; i++) {
  image = enhanceImage(image, i, algorithm);
  console.log(`after pass ${i + 1}`, {
    lit: getLitPixelCount(image),
    width: image[0].length,
    height: image.length,
  });
}
