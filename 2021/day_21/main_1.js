class Dice {
  val = 0;
  rolls = 0;
  roll() {
    if (this.val === 100) {
      this.val = 1;
    } else {
      this.val++;
    }
    this.rolls++;
    return this.val;
  }

  get rolls() {
    return this.rolls;
  }
}

class Board {
  positions = {};
  scores = {};

  constructor(starts) {
    this.positions = starts;
  }

  doSteps(player, count) {
    const currentPos = this.positions[player];
    const newPos = ((currentPos + count - 1) % 10) + 1;
    this.scores[player] = (this.scores[player] || 0) + newPos;
    this.positions[player] = newPos;

    console.log("moved player", { player, newPos, score: this.scores[player] });
  }

  determineWinner() {
    const winners = Object.entries(this.scores).filter(
      ([_, score]) => score >= 1000
    );
    return winners.length === 0 ? null : winners[0][0];
  }

  getScore(player) {
    return this.scores[player];
  }

  get scores() {
    return this.scores;
  }
}

const playGame = () => {
  let activePlayer = "1";
  const dice = new Dice();
  const board = new Board({ 1: 1, 2: 2 });

  while (true) {
    let count = 0;
    for (let i = 0; i < 3; i++) {
      count += dice.roll();
    }
    board.doSteps(activePlayer, count);
    const winner = board.determineWinner();
    if (winner) {
      const looser = winner === "1" ? "2" : "1";
      console.log({ res: board.scores[looser] * dice.rolls });
      return;
    }
    activePlayer = activePlayer === "1" ? "2" : "1";
  }
};

playGame();
