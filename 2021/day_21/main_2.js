const doSteps = (positions, scores, player, count) => {
  const currentPos = positions[player];
  const newPos = ((currentPos + count - 1) % 10) + 1;
  scores[player] = scores[player] + newPos;
  positions[player] = newPos;
};

const determineWinner = (scores) => {
  const winners = Object.entries(scores).filter(([_, score]) => score >= 21);
  return winners.length === 0 ? null : winners[0][0];
};

const determineOutcomeDistribution = () =>
  new Array(3)
    .fill(0)
    .flatMap((_, a) =>
      new Array(3)
        .fill(0)
        .flatMap((_, b) =>
          new Array(3).fill(0).flatMap((_, c) => a + b + c + 3)
        )
    )
    .map((val) => ({ [val]: 1 }))
    .reduce((a, b) => {
      const joined = { ...a };
      Object.entries(b).forEach(([key, val]) => {
        joined[key] = (joined[key] || 0) + val;
      });
      return joined;
    });

const outcomes = determineOutcomeDistribution();

const playGame = (activePlayer, universes, positions, scores) => {
  return Object.entries(outcomes)
    .flatMap(([outcome, newCount]) => {
      const newPositions = { ...positions };
      const newScores = { ...scores };
      doSteps(newPositions, newScores, activePlayer, Number(outcome));
      const winner = determineWinner(newScores);
      const universesAfterSplit = BigInt(newCount) * universes;
      if (winner) {
        return { [winner]: universesAfterSplit };
      }

      return playGame(
        activePlayer === "1" ? "2" : "1",
        universesAfterSplit,
        newPositions,
        newScores
      );
    })
    .reduce((a, b) => ({
      1: (a["1"] || BigInt(0)) + (b["1"] || BigInt(0)),
      2: (a["2"] || BigInt(0)) + (b["2"] || BigInt(0)),
    }));
};

const allWins = playGame("1", BigInt(1), { 1: 1, 2: 2 }, { 1: 0, 2: 0 });
console.log({ allWins, outcomes });
