const fs = require("fs");
const path = require("path");

const parsePos = (part) =>
  part
    .split("=")[1]
    .split("..")
    .map((n) => Number(n));

const importInstructions = (filePath) =>
  fs
    .readFileSync(path.resolve(__dirname, filePath))
    .toString()
    .split("\n")
    .map((line) => line.split(" "))
    .map((parts) => ({ type: parts[0], pos: parts[1].split(",") }))
    .map((val) => ({
      ...val,
      pos: {
        x: parsePos(val.pos[0]),
        y: parsePos(val.pos[1]),
        z: parsePos(val.pos[2]),
      },
    }));

const instructions = importInstructions("input_b.txt");
const enabledCubes = new Set();
const procedure = 50;
for (let {
  type,
  pos: { x: xRange, y: yRange, z: zRange },
} of instructions) {
  for (
    let x = Math.max(xRange[0], -procedure);
    x <= Math.min(xRange[1], procedure);
    x++
  ) {
    for (
      let y = Math.max(yRange[0], -procedure);
      y <= Math.min(yRange[1], procedure);
      y++
    ) {
      for (
        let z = Math.max(zRange[0], -procedure);
        z <= Math.min(zRange[1], procedure);
        z++
      ) {
        const key = `${x}.${y}.${z}`;
        if (type === "on") {
          enabledCubes.add(key);
        } else {
          enabledCubes.delete(key);
        }
      }
    }
  }
}
console.log({ result: enabledCubes.size });
