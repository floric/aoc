const fs = require("fs");
const path = require("path");

const parsePos = (part) =>
  part
    .split("=")[1]
    .split("..")
    .map((n) => Number(n));

const importInstructions = (filePath) =>
  fs
    .readFileSync(path.resolve(__dirname, filePath))
    .toString()
    .split("\n")
    .map((line) => line.split(" "))
    .map((parts) => ({ type: parts[0], pos: parts[1].split(",") }))
    .map((val) => ({
      ...val,
      pos: {
        x: parsePos(val.pos[0]),
        y: parsePos(val.pos[1]),
        z: parsePos(val.pos[2]),
      },
    }));

const createJoinedCubes = (a, b) => {};

const instructions = importInstructions("input_b.txt");
let joinedCubes = [];
for (let {
  type,
  pos: { x: xRange, y: yRange, z: zRange },
} of instructions) {
  console.log({ type, xRange, yRange, zRange });
  let newCubes = [];
  for (let cube of joinedCubes) {
    createJoinedCubes(cube, { xRange, yRange, zRange }).forEach((cube) =>
      joinedCubes.push(cube)
    );
  }
  joinedCubes = newCubes;
}
console.log({ result: 1 });
