use std::{
    fs::File,
    io::{BufReader, Read},
};

fn main() -> std::io::Result<()> {
    // read file to string
    let mut reader = BufReader::new(File::open("resources/data2.txt")?);
    let mut buffer = String::new();
    let _bytes = reader.read_to_string(&mut buffer);

    // parse elves
    let mut elves = buffer
        .split("\n\n")
        .enumerate()
        .map(|(id, elf_data)| {
            // parse calories
            let items = elf_data
                .split("\n")
                .filter(|val| !val.is_empty())
                .map(|val| val.parse::<u32>().expect("parse failed"))
                .collect::<Vec<_>>();
            // calculate sum
            let sum_calories = items.iter().sum::<u32>();
            (id, sum_calories, items)
        })
        .collect::<Vec<_>>();

    println!("found {} elves", elves.len());

    // sort descending
    elves.sort_by(|a, b| b.1.cmp(&a.1));

    // take first three and sum calories up
    let total_top_three = elves.iter().take(3).map(|elf| elf.1).sum::<u32>();

    println!("top three sum: {}", total_top_three);

    Ok(())
}
