use std::{
    fs::File,
    io::{BufReader, Read},
};

const ROCK_OPPONENT: &str = "A";
const PAPER_OPPONENT: &str = "B";
const SCISSORS_OPPONENT: &str = "C";
const ROCK_PLAYER: &str = "X";
const PAPER_PLAYER: &str = "Y";
const SCISSORS_PLAYER: &str = "Z";
const GOAL_DRAW: &str = "Y";
const GOAL_WIN: &str = "Z";

fn main() -> std::io::Result<()> {
    run_part_a()?;
    run_part_b()?;

    Ok(())
}

fn run_part_a() -> Result<(), std::io::Error> {
    let mut reader = BufReader::new(File::open("resources/data2.txt")?);
    let mut buffer = String::new();
    let _bytes = reader.read_to_string(&mut buffer);
    let total = buffer
        .split('\n')
        .map(|data| {
            let (opponent, player) = parse_data(data);
            let score = if player.eq(ROCK_PLAYER) && opponent.eq(SCISSORS_OPPONENT)
                || player.eq(PAPER_PLAYER) && opponent.eq(ROCK_OPPONENT)
                || player.eq(SCISSORS_PLAYER) && opponent.eq(PAPER_OPPONENT)
            {
                6
            } else if player.eq(ROCK_PLAYER) && opponent.eq(ROCK_OPPONENT)
                || player.eq(PAPER_PLAYER) && opponent.eq(PAPER_OPPONENT)
                || player.eq(SCISSORS_PLAYER) && opponent.eq(SCISSORS_OPPONENT)
            {
                3
            } else {
                0
            };
            let bonus = calculate_bonus(player);
            score + bonus
        })
        .sum::<i32>();

    println!("a) total {}", total);

    Ok(())
}

fn run_part_b() -> Result<(), std::io::Error> {
    let mut reader = BufReader::new(File::open("resources/data2.txt")?);
    let mut buffer = String::new();
    let _bytes = reader.read_to_string(&mut buffer);
    let total = buffer
        .split('\n')
        .map(|data| {
            let (opponent, goal) = parse_data(data);
            let score = match goal {
                GOAL_WIN => 6,
                GOAL_DRAW => 3,
                _ => 0,
            };
            let player = determine_player_response(opponent, goal);
            let bonus = calculate_bonus(&player);
            score + bonus
        })
        .sum::<i32>();

    println!("b) total {}", total);

    Ok(())
}

fn parse_data(data: &str) -> (&str, &str) {
    let line = data
        .split(' ')
        .filter(|val| !val.is_empty())
        .collect::<Vec<_>>();

    let left = line.get(0).unwrap().to_owned();
    let right = line.get(1).unwrap().to_owned();

    (left, right)
}

fn calculate_bonus(player: &str) -> i32 {
    match player {
        ROCK_PLAYER => 1,
        PAPER_PLAYER => 2,
        _ => 3,
    }
}

fn determine_player_response(opponent: &str, goal: &str) -> String {
    match opponent {
        SCISSORS_OPPONENT => match goal {
            GOAL_WIN => ROCK_PLAYER,
            GOAL_DRAW => SCISSORS_PLAYER,
            _ => PAPER_PLAYER,
        },
        ROCK_OPPONENT => match goal {
            GOAL_WIN => PAPER_PLAYER,
            GOAL_DRAW => ROCK_PLAYER,
            _ => SCISSORS_PLAYER,
        },
        _ => match goal {
            GOAL_WIN => SCISSORS_PLAYER,
            GOAL_DRAW => PAPER_PLAYER,
            _ => ROCK_PLAYER,
        },
    }
    .to_string()
}
