use itertools::Itertools;
use std::{
    collections::BTreeSet,
    fs::File,
    io::{BufReader, Read},
};

fn main() -> std::io::Result<()> {
    run_part_a()?;
    run_part_b()?;

    Ok(())
}

fn run_part_a() -> Result<(), std::io::Error> {
    let mut reader = BufReader::new(File::open("resources/data_a2.txt")?);
    let mut buffer = String::new();
    let _bytes = reader.read_to_string(&mut buffer);
    let total = buffer
        .split('\n')
        .map(|data| {
            let length = data.len() / 2;
            let (first, second) = data.split_at(length);
            let first_items = first.chars().collect::<BTreeSet<_>>();
            let second_items = second.chars().collect::<BTreeSet<_>>();
            let common_item = first_items
                .intersection(&second_items)
                .cloned()
                .next()
                .expect("missing common item");
            determine_priority(common_item)
        })
        .sum::<u32>();

    println!("a) total {}", total);

    Ok(())
}

fn run_part_b() -> Result<(), std::io::Error> {
    let mut reader = BufReader::new(File::open("resources/data_b2.txt")?);
    let mut buffer = String::new();
    let _bytes = reader.read_to_string(&mut buffer);
    let total = buffer
        .split('\n')
        .chunks(3)
        .into_iter()
        .map(|chunk| {
            let common_item = chunk
                .map(|line| line.chars().collect::<BTreeSet<_>>())
                .reduce(|a, b| a.intersection(&b).cloned().collect::<BTreeSet<_>>())
                .and_then(|common_items| common_items.iter().cloned().next())
                .expect("missing common item");

            determine_priority(common_item)
        })
        .sum::<u32>();

    println!("b) total {}", total);

    Ok(())
}

fn determine_priority(x: char) -> u32 {
    match x {
        'a'..='z' => x as u32 - 96,
        'A'..='Z' => x as u32 - 38,
        _ => panic!("unexpected input"),
    }
}
