use std::{
    fs::File,
    io::{BufReader, Read},
};

fn main() -> std::io::Result<()> {
    run_part_a()?;
    run_part_b()?;

    Ok(())
}

fn run_part_a() -> Result<(), std::io::Error> {
    let mut reader = BufReader::new(File::open("resources/data2.txt")?);
    let mut buffer = String::new();
    let _bytes = reader.read_to_string(&mut buffer);

    let mut value: i32 = 1;
    let mut sum = 0;
    let mut cycle = 0;
    buffer.split("\n").enumerate().for_each(|(_, command)| {
        let (cycles, change) = parse_command(command);

        for _ in 0..cycles {
            cycle += 1;
            if (cycle + 20) % 40 == 0 {
                println!("{}: {} -> {}", cycle, value, cycle as i32 * value);
                sum += cycle as i32 * value;
            }
        }
        value += change;
    });
    println!("sum: {}", sum);
    Ok(())
}

fn run_part_b() -> Result<(), std::io::Error> {
    let mut reader = BufReader::new(File::open("resources/data2.txt")?);
    let mut buffer = String::new();
    let _bytes = reader.read_to_string(&mut buffer);

    let mut sprite_start: i32 = 1;
    let mut cycle = 0;
    buffer.split("\n").enumerate().for_each(|(_, command)| {
        let (cycles, change) = parse_command(command);

        for _ in 0..cycles {
            cycle += 1;
            let is_black = cycle % 40 == sprite_start
                || cycle % 40 == sprite_start + 1
                || cycle % 40 == sprite_start + 2;
            print!("{}", if is_black { "#" } else { "." });
            if cycle % 40 == 0 {
                println!("");
            }
        }
        sprite_start += change;
    });

    Ok(())
}

fn parse_command(command: &str) -> (i32, i32) {
    let mut parts = command.split(' ');

    if parts.next().unwrap().eq("addx") {
        let change = parts.next().unwrap();

        return (2, change.parse::<i32>().expect("parsing failed"));
    }

    (1, 0)
}
